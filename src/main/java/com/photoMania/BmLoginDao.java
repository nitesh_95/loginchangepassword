package com.photoMania;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class BmLoginDao {
    @Autowired
    BmLoginRepository bmLoginRepository;
	
	public ManagerLogin checkingBmLogin(String userid) {
		
	return  bmLoginRepository.gettingBmCred(userid);
		 
	}
	public int updatePassword(String newpass, String solid) {

		return  bmLoginRepository.update(newpass,solid);		
	}


}
