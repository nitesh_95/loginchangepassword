package com.photoMania;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import org.slf4j.Logger;

import org.slf4j.LoggerFactory;
@CrossOrigin
@RestController
public class BmLoginController {
	
	Logger log = LoggerFactory.getLogger(BmLoginController.class);
	@Autowired
	BmLoginServiceImpl bmLoginServiceImpl;
	@RequestMapping("/login")
	public LoginReponse checkBmLoginCredentials(@RequestParam(name="UserId") String userid,@RequestParam(name="Password") String password) {
		log.info("userid::::::::"+userid);
		log.info("password::::::"+password);
		LoginReponse bm = bmLoginServiceImpl.fetchLoginDetailes(userid,password);
		return bm;
	}
	
	@RequestMapping("/changepassword")
	 public LoginReponse updatePassword(@RequestParam(name="Solid") String solid,@RequestParam(name="oldPassword") String oldpassword,@RequestParam(name="newPassword") String newpassword) {
		LoginReponse res=new LoginReponse();
		boolean isupdated= bmLoginServiceImpl.updateNewPassword(solid,newpassword,oldpassword);
		if(isupdated) {
			res.setReson("success");
			res.setStatus("00");
		}else {
			res.setReson("failed");
			res.setStatus("s01");
		}
		 return res;
	 }


}
