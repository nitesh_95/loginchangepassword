package com.photoMania;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PhotoMania1Application {

	public static void main(String[] args) {
		SpringApplication.run(PhotoMania1Application.class, args);
	}

}
