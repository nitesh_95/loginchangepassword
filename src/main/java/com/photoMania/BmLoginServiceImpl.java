package com.photoMania;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.slf4j.Logger;

import org.slf4j.LoggerFactory;

@Service
public class BmLoginServiceImpl implements BmLoginService {
	Logger log = LoggerFactory.getLogger(BmLoginServiceImpl.class);
	@Autowired
	BmLoginDao bmLoginDao;

	@Override
	public LoginReponse fetchLoginDetailes(String userid, String password) {
		ManagerLogin ss = gettingBmData(userid);
		LoginReponse reponse = new LoginReponse();
		try {
			if (ss != null) {
				TripleDES des = new TripleDES();
				String encryptpassword = des.encrypt(password);
				if (userid!=null && encryptpassword.equals(ss.getPassword())) {
					reponse.setSolid(ss.getSolid());
					reponse.setBranchname(ss.getBranchName());
					reponse.setPortaltype(ss.getPortaltype());
					reponse.setStatus("00");
					reponse.setReson("success");
				} else {
					reponse.setStatus("401");
					reponse.setReson("Invalid details");
				}

			} else {
				reponse.setStatus("s01");
				reponse.setReson("No Data Found");
			}
		} catch (Exception e) {
			e.getStackTrace();
		}
		return reponse;
	}

	@Override
	public ManagerLogin gettingBmData(String solid) {
		return bmLoginDao.checkingBmLogin(solid);
	}

	public boolean updateNewPassword(String solid, String newpassword, String oldpassword) {
		String encryptpassword = "";
		String encryptoldpassword = "";
		int count = 0;
		log.info("solid:::" + solid);
		log.info("newpass:::" + newpassword);
		log.info("oldpass::::"+oldpassword);
		try {
			TripleDES des = new TripleDES();
			ManagerLogin ss = gettingBmData(solid);
			encryptoldpassword = des.encrypt(oldpassword);
			log.info("encryptoldpassword:::::"+encryptoldpassword);
			log.info("dbpassword:::::"+ss.getPassword());
			if (encryptoldpassword.equals(ss.getPassword())) {
				encryptpassword = des.encrypt(newpassword);
				System.out.println("encrypt:::::::" + encryptpassword);
				System.out.println("solid:::::::" + solid);
				count = bmLoginDao.updatePassword(encryptpassword, solid);
				System.out.println("cou::::::::" + count);
			} else {
				System.out.println("not equal");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		if (count > 0) {
			return true;
		} else {
			return false;
		}
	}
	
}
