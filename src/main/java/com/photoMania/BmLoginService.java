package com.photoMania;



public interface BmLoginService {
 
	public LoginReponse fetchLoginDetailes(String userid, String password);
	
	public ManagerLogin gettingBmData(String solid);
	
	public boolean updateNewPassword(String solid, String newpassword, String oldpassword);
}
